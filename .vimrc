" Pathogen first
execute pathogen#infect()

"
" Various settings
"
filetype plugin indent on
syntax on
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set encoding=utf-8

"
" Remapping some keys
"
inoremap jk <ESC>
let mapleader = "\<Space>"


